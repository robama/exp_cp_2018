import csv

TIMEOUT = 300
results = {}

infile = '/home/roberto/exp_cp_2018/results/results_norn_gst'
reader = csv.reader(open(infile), delimiter = '|')
for row in reader:
  solv = row[0] + '_' + row[2]
  inst = row[3]
  inst = inst[:inst.find('.')]
  info = row[4]
  time = float(row[5])
  if info not in ['sat', 'unsat']:
    if time >= TIMEOUT:
      info = 'out'
    else:
      info = 'unk'
    time = TIMEOUT
  if inst not in results.keys():
    if row[1] == 'dec':
      results[inst] = {'dec': True}
    else:
      results[inst] = {'dec': False}
  results[inst][solv] = [info, time]
#  print inst, solv, info, time
  
insts = results.keys()
unsound = set([])
infile = '/home/roberto/exp_cp_2018/results/results_norn_smt'
reader = csv.reader(open(infile), delimiter = '|')
for row in reader:
  inst = row[1]
  inst = inst[:inst.find('.')]
  if inst not in insts:
    continue
  info = row[2]
  time = float(row[3])
  if info not in ['sat', 'unsat']:
    if time >= TIMEOUT:
      info = 'out'
    else:
      info = 'unk'
    time = TIMEOUT
  else:
    infos = set(
      [x[0] for x in results[inst].values() if not isinstance(x, bool) and 
      x[0] not in ['out', 'unk']]
    )
    if infos and (len(infos) != 1 or list(infos)[0] != info):
      print 'UNSOUND!', inst, row[0], info, time, results[inst]
      unsound.add(inst)
      assert False
  results[inst][row[0]] = [info, time]
#  print inst, row[0], info, time

infile = '/home/roberto/exp_cp_2018/results/results_norn_gec'
reader = csv.reader(open(infile), delimiter = '|')
for row in reader:
  solv = 'Gecode+S_' + row[1]
  inst = row[2]
  inst = inst[:inst.find('.')]
  info = row[3]
  time = float(row[4])
  if time >= TIMEOUT or info not in ['sat', 'unsat']:
    if time >= TIMEOUT:
      info = 'out'
    else:
      info = 'unk'
    time = TIMEOUT
  else:
    infos = set(
      [x[0] for x in results[inst].values() if not isinstance(x, bool) and 
      x[0] not in ['out', 'unk']]
    )
    if (infos and (len(infos) != 1 or list(infos)[0] != info)):
      print 'UNSOUND!', inst, solv, info, results[inst]
      unsound.add(inst)
      info = 'inc'
      time = TIMEOUT
#        assert False
  results[inst][solv] = [info, time]
#  print inst, row[0], info, time

n = 0.0
f = 0
info = {'sat': 0, 'unsat': 1} #FIXME: 1 unknown
solvers = (
  'G-Strings_decomp_500', 
  'G-Strings_decomp_1000', 
  'G-Strings_decomp_10000', 
  'G-Strings_notdec_500',
  'G-Strings_notdec_1000',
  'G-Strings_notdec_10000',
  'cvc4',
  'z3',
  'Gecode+S_500',
  'Gecode+S_1000',
  'Gecode+S_10000'
)
stats = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_sat = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_uns = dict((s, [0, 0.0, 0.0]) for s in solvers)
gec = set([])
m = len(solvers)
for inst, item in results.items():
  n += 1
#  if len(item) - 1 != m:
#    print 'Not all the solvers processed',inst,':',item.keys()
#    assert False
  if results[inst]['dec']:
    f += 1
  for s in solvers:
    if s in item.keys():
      it = item[s]
    else:
      it = ['unk', TIMEOUT]
      item[s] = it
    if s == 'Gecode+S_500' and it[0] in ['sat', 'unsat']:
      gec.add(inst)
    if it[0] in ['sat', 'unsat']:
      stats[s][0] += 1
      stats[s][1] += it[1]
      if s == 'G-Strings_decomp_500':
        info[it[0]] += 1
        if it[0] == 'sat':
          item['sat'] = True
        else:
          item['sat'] = False
      if item['sat']:
        stats_sat[s][0] += 1
        stats_sat[s][1] += it[1]
      else:
        stats_uns[s][0] += 1
        stats_uns[s][1] += it[1]
    else:
      if s == 'G-Strings_decomp_500':
         item['sat'] = False
      stats[s][1] += TIMEOUT
      if item['sat']:
        stats_sat[s][1] += TIMEOUT
      else:
        stats_uns[s][1] += TIMEOUT
  for i in range(0, m - 1):
    for j in range(i + 1, m):
      si = solvers[i]
      sj = solvers[j]
      ti = item[si][1]
      tj = item[sj][1]
      if ti < TIMEOUT:
        if tj < TIMEOUT:
          t = ti + tj
          if t == 0:
            stats[si][2] += 0.5
            stats[sj][2] += 0.5
          else:
            stats[si][2] += tj / t
            stats[sj][2] += ti / t
        else:
          stats[si][2] += 1
      elif tj < TIMEOUT:
        stats[sj][2] += 1
assert len(results) >= info['sat'] + info['unsat']
print 'Total instances:',int(n),'[',info['sat'],'sat,',info['unsat'],'unsat,',\
  int(n)-info['sat']-info['unsat'],'unknown,',f,'dec,',int(n-f),'non-dec ]'
print '*** SAT'
n = float(info['sat'])
for s, it in sorted(stats_sat.items(), key = lambda x : (-x[1][0], x[1][1], -x[1][2])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n,2), 'sec.' #  '-- borda:',it[2]
print '*** UNS'
n = float(info['unsat'])
for s, it in sorted(stats_uns.items(), key = lambda x : (-x[1][0], x[1][1], -x[1][2])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n,2), 'sec.' #  '-- borda:',it[2] 
print '*** TOT'
n = float(len(results))
for s, it in sorted(stats.items(), key = lambda x : (-x[1][0], x[1][1], -x[1][2])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n,2), 'sec.' #  '-- borda:',it[2]

info = {'sat': 0, 'unsat': 0}
stats = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_sat = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_uns = dict((s, [0, 0.0, 0.0]) for s in solvers)
for inst in gec:
  item = results[inst]
  for s, it in item.items():
    if isinstance(it, bool):
      continue
    if it[0] in ['sat', 'unsat']:
      stats[s][0] += 1
      stats[s][1] += it[1]
      if s == 'G-Strings_decomp_500':
        info[it[0]] += 1
    else:
      stats[s][1] += TIMEOUT
  for i in range(0, m - 1):
    for j in range(i + 1, m):
      si = solvers[i]
      sj = solvers[j]
      ti = item[si][1]
      tj = item[sj][1]
      if ti < TIMEOUT:
        if tj < TIMEOUT:
          t = ti + tj
          if True or t == 0: #FIXME:
            stats[si][2] += 0.5
            stats[sj][2] += 0.5
          else:
            stats[si][2] += tj / t
            stats[sj][2] += ti / t
        else:
          stats[si][2] += 1
      elif tj < TIMEOUT:
        stats[sj][2] += 1
n = float(len(gec))
print '\n**********\n'
print 'Instances correctly solved by Gecode+S:',int(n),'[',info['sat'],'sat,',info['unsat'],'unsat ]'
for s, it in sorted(stats.items(), key = lambda x : (-x[1][0], x[1][1])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100, 2),'% ] -- avg time', \
    round(it[1]/n,2),'sec.' # '-- borda:',it[2]
    
#n = 0
#for inst, item in results.items():
#  if item['sat'] and \
#  item['G-Strings_notdec_500'][1] - item['G-Strings_decomp_500'][1] > n:
#    n = item['G-Strings_notdec_500'][1] - item['G-Strings_decomp_500'][1]
#    i = inst
#print i, results[i]['G-Strings_decomp_500'], results[i]['G-Strings_notdec_500']


import csv

TIMEOUT = 300
results = {}
infile = '/home/roberto/exp_cp_2018/results/results_stranger_gst'
reader = csv.reader(open(infile), delimiter = '|')

for row in reader:
  solv = row[0] + '_' + row[1]
  inst = row[2]
  inst = inst[:inst.find('.')]
  info = row[3]
  time = float(row[4])
  if info not in ['sat', 'unsat']:
    if time >= TIMEOUT:
      info = 'out'
    else:
      info = 'unk'
    time = TIMEOUT
  if inst not in results.keys():
    results[inst] = {}
  results[inst][solv] = [info, time]
  
insts = results.keys()
infile = '/home/roberto/exp_cp_2018/results/results_stranger_smt'
reader = csv.reader(open(infile), delimiter = '|')
for row in reader:
  inst = row[1]
  inst = inst[:inst.find('.')]
  if inst not in insts:
    continue
  info = row[2]
  time = float(row[3])
  if time >= TIMEOUT or info not in ['sat', 'unsat']:
    if time >= TIMEOUT:
      info = 'out'
    else:
      info = 'unk'
    time = TIMEOUT
  else:
    infos = set(
      [x[0] for x in results[inst].values() if x[0] not in ['out', 'unk']]
    )
    if (infos and (len(infos) != 1 or list(infos)[0] != info)):
      if info == 'sat':
        print inst,'UNSOUND! unsat instead of sat: ',\
          [x[0] for x in results[inst].items() if x[1][0] == 'unsat']
      else:
        print inst,'UNSOUND! sat instead of unsat: ',\
          [x[0] for x in results[inst].items() if x[1][0] == 'sat']
      #assert False
  results[inst][row[0]] = [info, time]
  #print inst, row[0], info, time
 
#print 'Total unsound:', len(unsound)
n = 0.0
info = {'sat': 0, 'unsat': 0}
solvers = ['G-Strings_decomp_10000', 'G-Strings_notdec_10000', 'cvc4', 'z3']
stats = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_sat = dict((s, [0, 0.0, 0.0]) for s in solvers)
stats_uns = dict((s, [0, 0.0, 0.0]) for s in solvers)
m = len(solvers)
for inst, item in results.items():
  for s in solvers:
    it = item[s]
    if it[0] in ['sat', 'unsat']:
      stats[s][0] += 1
      stats[s][1] += it[1]
      if s == 'G-Strings_decomp_10000':
        info[it[0]] += 1
        if it[0] == 'sat':
          item['sat'] = True
        else:
          item['sat'] = False
      if item['sat']:
        stats_sat[s][0] += 1
        stats_sat[s][1] += it[1]
      else:
        stats_uns[s][0] += 1
        stats_uns[s][1] += it[1]
    else:
      stats[s][1] += TIMEOUT
      if item['sat']:
        stats_sat[s][1] += TIMEOUT
      else:
        stats_uns[s][1] += TIMEOUT
  for i in range(0, m - 1):
    for j in range(i + 1, m):
      si = solvers[i]
      sj = solvers[j]
      ti = item[si][1]
      tj = item[sj][1]
      if ti < TIMEOUT:
        if tj < TIMEOUT:
          t = ti + tj
          if True or t == 0:
            stats[si][2] += 0.5
            stats[sj][2] += 0.5
          else:
            stats[si][2] += tj / t
            stats[sj][2] += ti / t
        else:
          stats[si][2] += 1
      elif tj < TIMEOUT:
        stats[sj][2] += 1
assert len(results) == info['sat'] + info['unsat']
print 'Total instances:',len(results),'[',info['sat'],'sat,',info['unsat'],'unsat ]'
print '*** SAT'
n = float(info['sat'])
for s, it in sorted(stats_sat.items(), key = lambda x : (-x[1][0], x[1][1])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n, 2),'sec.'
print '*** UNS'
n = float(info['unsat'])
for s, it in sorted(stats_uns.items(), key = lambda x : (-x[1][0], x[1][1])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n, 2),'sec.'
print '*** TOT'
n = float(len(results))
for s, it in sorted(stats.items(), key = lambda x : (-x[1][0], x[1][1])):
  print s,'-- solved:',it[0],'[',round(it[0]/n*100,2),'% ] -- avg time', \
    round(it[1]/n, 2),'sec.' #' -- borda:',it[2]
  



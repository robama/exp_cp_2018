#!/usr/bin/env python
from string import whitespace
import sys
import os.path
import optparse as op

class ParseError(Exception):
  def __init__(self, token, msg):
    self.token = token
    self.msg = msg
  def __str__(self):
    return self.msg + ": " + repr(self.token)

atom_end = set('()"\'') | set(whitespace)

def parse(sexp):
    stack, i, length = [[]], 0, len(sexp)
    while i < length:
        c = sexp[i]
        
        #print c, stack
        reading = type(stack[-1])
        if reading == list:
            if   c == '(': stack.append([])
            elif c == ')':
                stack[-2].append(stack.pop())
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
            elif c == '"': stack.append('')
            elif c == "'": stack.append([('quote',)])
            elif c in whitespace: pass
            else: stack.append((c,))
        elif reading == str:
            if   c == '"':
                stack[-2].append(stack.pop())
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
            elif c == '\\':
                i += 1
                stack[-1] += sexp[i]
            else: stack[-1] += c
        elif reading == tuple:
            if c in atom_end:
                atom = stack.pop()
                if atom[0][0].isdigit(): stack[-1].append(eval(atom[0]))
                else: stack[-1].append(atom)
                if stack[-1][0] == ('quote',): stack[-2].append(stack.pop())
                continue
            else: stack[-1] = ((stack[-1][0] + c),)
        i += 1
    return stack.pop()
class GecodeAggregateModel:
    def __init__(self,file,parsetree):
        self.file = file
        self.re, self.cstr = [],[]
        self.const = {}
        self.var = {}
        self.lvar = set()
        self.tempcount = 0
        self.regvars = {}
        self.parsetree = parsetree
        for line in parsetree:
            exptype = line[0][0]
            if exptype == 'set-logic': 
                pass
            elif exptype == 'declare-fun':
                # self.var[varname] = (constraint count, alphabet)
                self.var[line[1][0]] = (0,'Full')
            elif exptype == 'check-sat':
                pass
            elif exptype == 'assert':
                self.process_constraint(line[1])
            else:
                raise ParseError(line,'Unknown expression type')
        return
  
    def process_constraint(self, constraint):
        if constraint[0][0] == 'str.in.re':
            varname = self.process_strexp(constraint[1])
            re = self.process_reexp(constraint[2])
            if re == '':
                # regexp accepting the only empty string
                self.cstr.append('\trel(*this,len_{},0);\n'.format(varname))
                self.useVar(varname)
            else:
                dfanum = len(self.re)
                self.re.append('\tREG r_{0} = {1} + *REG(-1);\n\tDFA d_{0}(r_{0});\n'.format(dfanum,re))
                self.cstr.append('\textensional(*this,{},d_{});\n'.format(varname,dfanum))
                self.useVar(varname, 'a_{}'.format(dfanum))
        elif constraint[0][0] == 'not':
            if constraint[1][0][0] == 'str.in.re':
                varname = self.process_strexp(constraint[1][1])
                re = self.process_reexp(constraint[1][2])
                if re == '':
                    if varname.startswith(('var','temp')):
                        # varname \notin emptystring
                        lvarname = 'len_' + varname
                        #self.lvar = self.lvar | {lvarname}
                        #self.cstr.append('\tlength(*this,{},{});\n'.format(varname,lvarname))
                        self.useVar(varname)
                        self.cstr.append('\trel(*this, 1 <= len_{} );\n'.format(lvarname))
                    else:
                        dfanum = len(self.re)
                        self.re.append('\tREG r_{0};\n\tDFA d_{0}(r_{0});\n'.format(dfanum))
                        self.cstr.append('\textensional(*this,{},d_{});\n'.format(varname,dfanum))
                else:
                    if varname.startswith(('var','temp')):
                        raise ParseError(constraint,"Negated regexp not supported")
                        dfanum = len(self.re)
                        self.re.append('\tREG r_{0} = {1};\n\tDFA d_{0}(r_{0});\n'.format(dfanum,re))
                        negdfanum = len(self.re)
                        self.re.append('\tDFA d_{0} = negate_dfa(d_{1});\n\tBitSet a_{0} = alphabetof(d_{0});\n'.format(negdfanum,dfanum))
                        self.cstr.append('\textensional(*this,{},d_{});\n'.format(varname,negdfanum))
                        self.useVar(varname, 'a_{}'.format(dfanum))
                    else:
                        raise ParseError(constraint,"Negated regexp not supported")
            elif constraint[1][0][0] == 'not':
                self.process_constraint(constraint[1][1])
        elif constraint[0][0] == 'and':
            self.process_constraint(constraint[1])
            self.process_constraint(constraint[2])
        elif constraint[0][0] == '<=':
            cstr = self.process_relation(constraint)
            self.cstr.append('\trel(*this, {});\n'.format(cstr))
        elif constraint[0][0] == '=':
            cstr = self.process_relation(constraint)
            self.cstr.append('\trel(*this, {});\n'.format(cstr))
        elif constraint[0][0] == 'exists':
            wit, rel = constraint[1][0], constraint[2]
            if wit[1][0] == 'Int':
                witness = wit[0][0]
                #witnessvar = 'temp_{}_{}'.format(self.tempcount,witness)
                #self.tempcount += 1
                self.lvar = self.lvar | {witness}
            else:
                raise ParseError(constraint,"Unhandled witness type")
            relation = self.process_relation(rel)
            self.cstr.append('\trel(*this,{});\n'.format(relation))
        else:
            raise ParseError(constraint, "Unrecognized constraint")
            self.cstr.append('\t{};\n'.format(constraint))

    def process_relation(self,relexp):
        if relexp[0][0] == '<=':
            lhs = self.process_numexp(relexp[1])
            rhs = self.process_numexp(relexp[2])
            return '{} <= {}'.format(lhs,rhs)
        elif relexp[0][0] == '=':
            lhs = self.process_numexp(relexp[1])
            rhs = self.process_numexp(relexp[2])
            return '{} == {}'.format(lhs,rhs)
        raise ParseError(relexp,"Unhandled relation type")

    def process_numexp(self,numexp):
        #print "process_numexp::{}".format(numexp)
        exptype = type(numexp)
        if exptype == int:
            return numexp
        elif exptype == tuple:
            return numexp[0]
        elif exptype == list:
            if numexp[0][0] == '+':
                lhs = self.process_numexp(numexp[1])
                rhs = self.process_numexp(numexp[2])
                return '({}) + ({})'.format(lhs,rhs)
            elif numexp[0][0] == '*':
                lhs = self.process_numexp(numexp[1])
                rhs = self.process_numexp(numexp[2])
                return '({}) * ({})'.format(lhs,rhs)
            elif numexp[0][0] == 'str.len':
                varname = numexp[1][0]
                lvarname = 'len_' + varname
                #self.lvar = self.lvar | {lvarname}
                #self.cstr.append('\tlength(*this,{},{});\n'.format(varname,lvarname))
                self.useVar(varname)
                return lvarname
            elif numexp[0][0] == '-':
                return -1 * numexp[1]
        return numexp
    
    def process_strexp(self,strexp):
        #print "process_strexp::{}".format(strexp)
        exptype = type(strexp)
        if exptype == tuple:
            # strexp is a single string var
            return strexp[0]
        elif exptype == list:
            # strexp is compound
            if strexp[0][0] == 'str.++':
                # convert to concat constraints
                arglist = strexp[1:]
                while len(arglist) > 1:
                    newarglist = []
                    while len(arglist) > 1:
                        if ((type(arglist[0]) == tuple) or (type(arglist[1]) == tuple)):
                            varname = self.addTemp()
                            arg = [0,0]
                            for pos in [0,1]:
                                if type(arglist[pos]) == tuple:
                                    # argument is a string var
                                    arg[pos] = arglist[pos][0]
                                    self.useVar(arg[pos])
                                else:
                                    # argument is a const string
                                    arg[pos] = self.addConst(arglist[pos])
                                    #arg[pos] = 'str2IntArgs("{}")'.format(arglist[pos])
                            self.cstr.append('\topen_concat(*this,{0},len_{0},{1},len_{1},{2},len_{2});\n'.format(arg[0],arg[1],varname))
                            self.useVar(varname)
                            newarglist.append((varname,))
                        else:
                            newarglist.append('{}{}'.format(arglist[0],arglist[1]))
                        arglist = arglist[2:]
                    if len(arglist) > 0:
                        newarglist.append(arglist[0])
                    arglist = newarglist
                if type(arglist[0]) == tuple:
                    return arglist[0][0]
                else:
                    return self.addConst(arglist[0])
            else:
                raise ParseError(strexp,'Unknown string expression type')
        elif exptype == str:
            return self.addConst(strexp)
        raise ParseError(strexp,'Unrecognized string expression format')
        return strexp
    
    def addConst(self,const_str):
        varname = 'const_' + str(len(self.const))
        decl = "\tIntVarArray {}(*this,opt.length());\n".format(varname)
        for i in range(len(const_str)):
            decl += "\t{0}[{1}] = IntVar(*this,char2val('{2}'),char2val('{2}'));\n".format(varname,i,const_str[i])
        decl += "\tfor (unsigned int i = {}; i < {}.size();i++)\n".format(len(const_str),varname)
        decl += "\t\t{}[i] = IntVar(*this,-1,-1);\n".format(varname)
        decl += "\tIntVar len_{0}(*this,{1},{1});\n".format(varname,len(const_str))
        decl += "\t_l << len_{0}; _x << {0};\n".format(varname)
        self.const[varname] = decl
        return varname

    def addTemp(self):
        varname = 'temp_' + str(self.tempcount)
        self.var[varname] = (1,'Full')
        self.tempcount = self.tempcount + 1
        return varname

    def useVar(self,varname,alpha=None):
        if varname.startswith(('var','temp')):
            count = self.var[varname][0] + 1
            if alpha is not None:
                self.var[varname] = (count, 'interof({},{})'.format(self.var[varname][1],alpha))
            else:
                self.var[varname] = (count, self.var[varname][1])
        
    def process_reexp(self,reexp):
        #print "process_reexp::{}".format(reexp)
        if type(reexp) == tuple:
#            FIXME: re.nostr is not is the regular expression rejecting every string
#            if reexp[0] == 're.nostr':
#                return ''
            raise ParseError(reexp, "Unrecognized regexp")
        elif type(reexp) == list:
            if reexp[0][0] == 'str.to.re':
                if reexp[1] == '':
                    return ''
                return "REG(char2val('{}'))".format(reexp[1])
            elif reexp[0][0] == 're.union':
                lhs = self.process_reexp(reexp[1])
                rhs = self.process_reexp(reexp[2])
                if lhs == '':
                    return rhs
                elif rhs == '':
                    return lhs
                else:
                    return '({}) | ({})'.format(lhs,rhs)
            elif reexp[0][0] == 're.*':
                subexp = self.process_reexp(reexp[1])
                if subexp == '':
                    return ''
                return '*({})'.format(subexp)
            elif reexp[0][0] == 're.++':
                lhs = self.process_reexp(reexp[1])
                rhs = self.process_reexp(reexp[2])
                if lhs == '':
                    return rhs
                elif rhs == '':
                    return lhs
                else:
                    return '({}) + ({})'.format(lhs,rhs)
            elif reexp[0][0] == 're.range':
                return "REG(IntArgs::create(char2val('{1}')-char2val('{0}')+1,char2val('{0}')))".format(reexp[1],reexp[2])
#            FIXME: re.nostr is not is the regular expression rejecting every string
#            elif reexp[0][0] == 're.nostr':
#                # regexp accepting the empty string:
#                return ''
        raise ParseError(reexp,"Regular expression not recognized")
        return reexp
    
    def __str__(self):
        retval = '/*\n'
        for line in self.parsetree:
            retval += '{}\n'.format(line)
        retval += '*/\n'
        retval += '''
#include "aggregate-model.hh"

class Benchmark : public AggregateModel {
public:
  Benchmark(const AggregateOptions& opt)
  : AggregateModel(opt)
  {    
'''
        retval = retval + "\t//DFAs:\n"
        for line in self.re:
            retval = retval + line
        retval = retval + '\t//Variables:\n'
        retval += '\tIntVarArgs _x; IntVarArgs _l;\n'
        for v,p in self.var.iteritems():
            if p[0] != 0:
                retval += \
                '\tIntVarArray {0}(*this,opt.length(),-1,opt.width());\n\t_x << {0};\n'.format(v)
                retval += \
                '\tIntVar len_{0}(*this,opt.minlength(),opt.length());\n\t_l << len_{0};\n'.format(v)
        for l in self.lvar:
            retval = retval + '\tIntVar {0}(*this,opt.minlength(),opt.length());\n\t_l << {0};\n'.format(l)
        for v,c in self.const.iteritems():
            retval += c
        retval += '\tx = IntVarArray(*this,_x);\n\tl = IntVarArray(*this,_l);\n'
        retval = retval + '\t//Constraints:\n'
        for v,p in self.var.iteritems():
            if p[0] != 0:
                retval += '\topen_invariant(*this,{0},len_{0});\n'.format(v)
        for line in self.cstr:
            retval = retval + line
        retval += '''
    
        post_brancher(x,opt);
      }
  
    	Benchmark(bool share, Benchmark& s)
        : AggregateModel(share,s) {}

    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
  
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };

    int main(int argc, char* argv[]) {
'''
        retval += '\tAggregateOptions opt("Benchmark::{}");\n'.format(self.file)
        retval += '''
      opt.solutions(1);
      opt.min(65);
      AggregateModel::standardOptions(opt);
      opt.parse(argc,argv);

      Script::run<Benchmark,DFS,AggregateOptions>(opt);
    	return 0;
    }
    '''
        return retval


class GecodeStringVarModel:
  def __init__(self,file,parsetree):
    self.file = file
    self.re, self.cstr = [],[]
    self.var = {}
    self.lvar = set()
    self.tempcount = 0
    self.regvars = {}
    self.parsetree = parsetree
    for line in parsetree:
      exptype = line[0][0]
      if exptype in ['set-logic', 'check-sat', 'set-option', 'get-model']:
        pass
      elif exptype == 'declare-fun':
        # self.var[varname] = (constraint count, alphabet)
        self.var[line[1][0]] = (0,'Full')
      elif exptype == 'assert':
        self.process_constraint(line[1])
      else:
        raise ParseError(line,'Unknown expression type')
    return
  
  def process_constraint(self, constraint):
    if constraint[0][0] == 'str.in.re':
      varname = self.process_strexp(constraint[1])
      re = self.process_reexp(constraint[2])
      if re == '':
        # regexp accepting the only empty string
        self.cstr.append('\tlength(*this,{},0);\n'.format(varname))
        self.useVar(varname)
      else:
        dfanum = len(self.re)
        self.re.append( \
          '\tREG r_{0} = {1};\n\tDFA d_{0}(r_{0});\n\tBitSet a_{0} = alphabetof(d_{0});\n'.format(dfanum,re))
        self.cstr.append( \
          '\textensional(*this,{},d_{});\n'.format(varname,dfanum))
        self.useVar(varname, 'a_{}'.format(dfanum))
    elif constraint[0][0] == 'not':
      if constraint[1][0][0] == 'str.in.re':
        varname = self.process_strexp(constraint[1][1])
        re = self.process_reexp(constraint[1][2])
        if re == '':
          if varname.startswith(('var','temp')):
            # varname \notin emptystring
            lvarname = 'len_' + varname
            self.lvar = self.lvar | {lvarname}
            self.cstr.append('\tlength(*this,{},{});\n'.format(varname,lvarname))
            self.useVar(varname)
            self.cstr.append('\trel(*this, 1 <= {} );\n'.format(lvarname))
          else:
            dfanum = len(self.re)
            self.re.append('\tREG r_{0};\n\tDFA d_{0}(r_{0});\n'.format(dfanum))
            self.cstr.append('\textensional(*this,{},d_{});\n'.format(varname,dfanum))
        else:
          if varname.startswith(('var','temp')):
            raise ParseError(constraint,"Negated regexp not supported")
            dfanum = len(self.re)
            self.re.append( \
              '\tREG r_{0} = {1};\n\tDFA d_{0}(r_{0});\n\tBitSet a_{0} = alphabetof(d_{0});\n'.format(dfanum,re))
            negdfanum = len(self.re)
            self.re.append( \
              '\tDFA d_{0} = negate_dfa(d_{1});\n\tBitSet a_{0} = alphabetof(d_{0});\n'.format(negdfanum,dfanum))
            self.cstr.append('\textensional(*this,{},d_{});\n'.format(varname,negdfanum))
            self.useVar(varname, 'a_{}'.format(dfanum))
          else:
            raise ParseError(constraint,"Negated regexp not supported")
      elif constraint[1][0][0] == 'not':
        self.process_constraint(constraint[1][1])
    elif constraint[0][0] == 'and':
      self.process_constraint(constraint[1])
      self.process_constraint(constraint[2])
    elif constraint[0][0] == '<=':
      cstr = self.process_relation(constraint)
      self.cstr.append('\trel(*this, {});\n'.format(cstr))
    elif constraint[0][0] == '=':
      cstr = self.process_relation(constraint)
      self.cstr.append('\trel(*this, {});\n'.format(cstr))
    elif constraint[0][0] == 'exists':
      wit, rel = constraint[1][0], constraint[2]
      if wit[1][0] == 'Int':
        witness = wit[0][0]
        #witnessvar = 'temp_{}_{}'.format(self.tempcount,witness)
        #self.tempcount += 1
        self.lvar = self.lvar | {witness}
      else:
        raise ParseError(constraint,"Unhandled witness type")
      relation = self.process_relation(rel)
      self.cstr.append('\trel(*this,{});\n'.format(relation))
    else:
      #raise ParseError(constraint, "Unrecognized constraint")
      self.cstr.append('\t{};\n'.format(constraint))
  
  def process_relation(self,relexp):
    if relexp[0][0] == '<=':
      lhs = self.process_numexp(relexp[1])
      rhs = self.process_numexp(relexp[2])
      return '{} <= {}'.format(lhs,rhs)
    elif relexp[0][0] == '=':
      lhs = self.process_numexp(relexp[1])
      rhs = self.process_numexp(relexp[2])
      return '{} == {}'.format(lhs,rhs)
    raise ParseError(relexp,"Unhandled relation type")
  
  def process_numexp(self,numexp):
    exptype = type(numexp)
    if exptype == int:
      return numexp
    elif exptype == tuple:
      return numexp[0]
    elif exptype == list:
      if numexp[0][0] == '+':
        lhs = self.process_numexp(numexp[1])
        rhs = self.process_numexp(numexp[2])
        return '({}) + ({})'.format(lhs,rhs)
      elif numexp[0][0] == '*':
        lhs = self.process_numexp(numexp[1])
        rhs = self.process_numexp(numexp[2])
        return '({}) * ({})'.format(lhs,rhs)
      elif numexp[0][0] == 'str.len':
        varname = numexp[1][0]
        lvarname = 'len_' + varname
        self.lvar = self.lvar | {lvarname}
        self.cstr.append('\tlength(*this,{},{});\n'.format(varname,lvarname))
        self.useVar(varname)
        return lvarname
      elif numexp[0][0] == '-':
        return -1 * numexp[1]
    return numexp
  
  def process_strexp(self,strexp):
    exptype = type(strexp)
    if exptype == tuple:
      # strexp is a single string var
      return strexp[0]
    elif exptype == list:
      # strexp is compound
      if strexp[0][0] == 'str.++':
        # convert to concat constraints
        arglist = strexp[1:]
        while len(arglist) > 1:
          newarglist = []
          while len(arglist) > 1:
            if ((type(arglist[0]) == tuple) or (type(arglist[1]) == tuple)):
              varname = self.addTemp()
              arg = [0,0]
              for pos in [0,1]:
                if type(arglist[pos]) == tuple:
                  # argument is a string var
                  arg[pos] = arglist[pos][0]
                  self.useVar(arg[pos])
                else:
                  # argument is a const string
                  arg[pos] = 'str2IntArgs("{}")'.format(arglist[pos])
              self.cstr.append('\tconcat(*this,{},{},{});\n'.format(arg[0],arg[1],varname))
              self.useVar(varname)
              newarglist.append((varname,))
            else:
              newarglist.append('{}{}'.format(arglist[0],arglist[1]))
            arglist = arglist[2:]
          if len(arglist) > 0:
            newarglist.append(arglist[0])
          arglist = newarglist
        if type(arglist[0]) == tuple:
          return arglist[0][0]
        else:
          return 'str2IntArgs("{}")'.format(arglist[0])
      else:
        raise ParseError(strexp,'Unknown string expression type')
    elif exptype == str:
      return 'str2IntArgs("{}")'.format(strexp)
    raise ParseError(strexp,'Unrecognized string expression format')
    return strexp
  
  def addTemp(self):
    varname = 'temp_' + str(self.tempcount)
    self.var[varname] = (1,'Full')
    self.tempcount = self.tempcount + 1
    return varname
  
  def useVar(self,varname,alpha=None):
    if varname.startswith(('var','temp')):
      count = self.var[varname][0] + 1
      if alpha is not None:
        self.var[varname] = (count, 'interof({},{})'.format(self.var[varname][1],alpha))
      else:
        self.var[varname] = (count, self.var[varname][1])
  
  def process_reexp(self,reexp):
    if type(reexp) == tuple:
      #FIXME: re.nostr is not is the regular expression rejecting every string
      #if reexp[0] == 're.nostr':
        #return ''
      raise ParseError(reexp, "Unrecognized regexp")
    elif type(reexp) == list:
      if reexp[0][0] == 'str.to.re':
        if reexp[1] == '':
          return ''
        return "REG(char2val('{}'))".format(reexp[1])
      elif reexp[0][0] == 're.union':
        lhs = self.process_reexp(reexp[1])
        rhs = self.process_reexp(reexp[2])
        if lhs == '':
          return rhs
        elif rhs == '':
          return lhs
        else:
          return '({}) | ({})'.format(lhs,rhs)
      elif reexp[0][0] == 're.*':
        subexp = self.process_reexp(reexp[1])
        if subexp == '':
          return ''
        return '*({})'.format(subexp)
      elif reexp[0][0] == 're.++':
        lhs = self.process_reexp(reexp[1])
        rhs = self.process_reexp(reexp[2])
        if lhs == '':
          return rhs
        elif rhs == '':
          return lhs
        else:
          return '({}) + ({})'.format(lhs,rhs)
      elif reexp[0][0] == 're.range':
        return "REG(IntArgs::create(char2val('{1}')-char2val('{0}')+1,char2val('{0}')))".format(reexp[1],reexp[2])
#      elif reexp[0][0] == 're.nostr':
#        # regexp accepting the empty string:
#        FIXME: re.nostr is not is the regular expression rejecting every string
#        return ''
    raise ParseError(reexp,"Regular expression not recognized")
    return reexp
  
  def __str__(self):
    retval = '/*\n'
    for line in self.parsetree:
      retval += '{}\n'.format(line)
    retval += '*/\n'
    retval += '''
#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
'''
    retval = retval + "\t//DFAs:\n"
    for line in self.re:
      retval = retval + line
    retval = retval + '\t//Variables:\n'
    retval += '\tStringVarArgs _x; IntVarArgs _l;\n'
    for v,p in self.var.iteritems():
      if p[0] != 0:
        retval = retval + '\tStringVar {0}(*this,opt.minlength(),opt.length(),opt.width(),{1},opt.block());\n\t_x << {0};\n'.format(v,p[1])
    for l in self.lvar:
      retval = retval + '\tIntVar {0}(*this,opt.minlength(),opt.length());\n\t_l << {0};\n'.format(l)
    retval += '\tx = StringVarArray(*this,_x);\n\tl = IntVarArray(*this,_l);\n'
    retval = retval + '\t//Constraints:\n'
    for line in self.cstr:
      retval = retval + line
    retval += '''
        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
'''
    retval += '\tStringOptions opt("Benchmark::{}", atoi(argv[1]));\n'.format(self.file)
    retval += '''
      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    '''
    return retval

if __name__ == "__main__":
  parser = op.OptionParser()
  parser.add_option('-a', '--aggregate', help='boolean option', \
      dest='aggregate', default=False, action='store_true')
  parser.add_option('-s', '--stdout', help='boolean option', \
      dest='stdout', default=False, action='store_true')
  (opts, args) = parser.parse_args()
  read, generated = 0, 0
  for file in args:
    f = open(file,"r")
    read = read + 1
    basename, extension = os.path.splitext(file)
    parsetree = parse(f.read())
    #for line in parsetree:
    #  print line
    try:
      if opts.aggregate:
        model = GecodeAggregateModel(basename, parsetree)
        outname = basename + '-agg.cpp'
      else:
        model = GecodeStringVarModel(basename, parsetree)
        outname = basename + '.cpp'
      if opts.stdout:
        print(str(model))
      else:
        fout = open(outname, 'w')
        fout.write(str(model))
        fout.close()
      generated = generated + 1
    except ParseError as e:
      print "Parse error ({}):\n{}\n".format(file,e)
    f.close()
  print "{} out of {} models generated".format(generated,read)
  

/*
[('set-logic',), ('QF_S',)]
[('set-option',), (':produce-models',), ('true',)]
[('declare-fun',), ('selKeyword_value',), [], ('String',)]
[('declare-fun',), ('selKeyword_value_trimed',), [], ('String',)]
[('declare-fun',), ('p1',), [], ('String',)]
[('declare-fun',), ('p2',), [], ('String',)]
[('declare-fun',), ('p3',), [], ('String',)]
[('assert',), [('=',), ('selKeyword_value',), [('str.++',), ('p1',), ('p2',), ('p3',)]]]
[('assert',), [('str.in.re',), ('p1',), [('re.*',), [('re.union',), [('str.to.re',), ' '], [('str.to.re',), 't']]]]]
[('assert',), [('str.in.re',), ('p3',), [('re.*',), [('re.union',), [('str.to.re',), ' '], [('str.to.re',), 't']]]]]
[('assert',), [('not',), [('str.suffixof',), ' ', ('p2',)]]]
[('assert',), [('not',), [('str.suffixof',), 't', ('p2',)]]]
[('assert',), [('not',), [('str.prefixof',), 't', ('p2',)]]]
[('assert',), [('not',), [('str.prefixof',), ' ', ('p2',)]]]
[('assert',), [('=',), ('selKeyword_value_trimed',), ('p2',)]]
[('assert',), [('=',), 't tLxxxx29886 t', ('selKeyword_value',)]]
[('check-sat',)]
[('get-model',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = *((REG(char2val(' '))) | (REG(char2val('t'))));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *((REG(char2val(' '))) | (REG(char2val('t'))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	rel(*this, selKeyword_value == [('str.++',), ('p1',), ('p2',), ('p3',)]);
	extensional(*this,p1,d_0);
	extensional(*this,p3,d_1);
	rel(*this, selKeyword_value_trimed == p2);
	rel(*this, t tLxxxx29886 t == selKeyword_value);

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/appscan/t08", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

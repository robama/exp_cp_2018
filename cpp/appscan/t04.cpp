/*
[('set-logic',), ('QF_S',)]
[('set-option',), (':produce-models',), ('true',)]
[('declare-fun',), ('cookie',), [], ('String',)]
[('declare-fun',), ('cookie_part1',), [], ('String',)]
[('declare-fun',), ('cookie_part2',), [], ('String',)]
[('declare-fun',), ('cookie_part3',), [], ('String',)]
[('declare-fun',), ('t1',), [], ('String',)]
[('declare-fun',), ('l1',), [], ('String',)]
[('assert',), [('=',), ('cookie',), [('str.++',), 'expires=Thu, 18 Dec 2013 12:00:00 UTC;searchLang=', ('t1',), 'domain=www.somesite.com']]]
[('assert',), [('=',), ('cookie',), [('str.++',), ('cookie_part1',), ('cookie_part2',), ('cookie_part3',)]]]
[('assert',), [('str.in.re',), ('cookie_part2',), [('re.++',), [('re.union',), [('str.to.re',), '?'], [('str.to.re',), ';']], [('str.to.re',), 'searchLang='], [('re.*',), [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'b'], [('str.to.re',), 'c'], [('str.to.re',), 'd'], [('str.to.re',), 'e'], [('str.to.re',), 'f'], [('str.to.re',), 'g'], [('str.to.re',), 'h'], [('str.to.re',), 'i'], [('str.to.re',), 'j'], [('str.to.re',), 'k'], [('str.to.re',), 'l'], [('str.to.re',), 'm'], [('str.to.re',), 'n']]]]]]
[('assert',), [('=>',), [('not',), [('=',), '', ('cookie_part3',)]], [('=',), ('cookie_part3',), [('str.++',), ';', ('l1',)]]]]
[('assert',), [('>',), [('str.len',), ('cookie_part2',)], 12]]
[('assert',), [('not',), [('=',), '', ('cookie_part1',)]]]
[('check-sat',)]
[('get-model',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = ((REG(char2val('?'))) | (REG(char2val(';')))) + (REG(char2val('searchLang=')));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	rel(*this, cookie == [('str.++',), 'expires=Thu, 18 Dec 2013 12:00:00 UTC;searchLang=', ('t1',), 'domain=www.somesite.com']);
	rel(*this, cookie == [('str.++',), ('cookie_part1',), ('cookie_part2',), ('cookie_part3',)]);
	extensional(*this,cookie_part2,d_0);
	[('=>',), [('not',), [('=',), '', ('cookie_part3',)]], [('=',), ('cookie_part3',), [('str.++',), ';', ('l1',)]]];
	[('>',), [('str.len',), ('cookie_part2',)], 12];

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/appscan/t04", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), ('var_3',), 'z', ('var_4',)], [('re.++',), [('re.*',), [('re.union',), [('str.to.re',), 'z'], [('re.++',), [('re.union',), [('str.to.re',), 'b'], [('str.to.re',), 'a']], [('re.union',), [('str.to.re',), 'z'], [('str.to.re',), 'b']]]]], [('re.union',), [('str.to.re',), 'b'], [('str.to.re',), 'a']]]]]
[('assert',), [('str.in.re',), ('var_4',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), ('var_4',), [('re.*',), [('str.to.re',), 'b']]]]
[('assert',), [('str.in.re',), ('var_3',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), ('var_3',), [('re.*',), [('str.to.re',), 'a']]]]
[('assert',), [('<=',), 0, [('str.len',), ('var_4',)]]]
[('check-sat',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = (*((REG(char2val('z'))) | (((REG(char2val('b'))) | (REG(char2val('a')))) + ((REG(char2val('z'))) | (REG(char2val('b'))))))) + ((REG(char2val('b'))) | (REG(char2val('a'))));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	REG r_2 = *(REG(char2val('b')));
	DFA d_2(r_2);
	BitSet a_2 = alphabetof(d_2);
	REG r_3 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_3(r_3);
	BitSet a_3 = alphabetof(d_3);
	REG r_4 = *(REG(char2val('a')));
	DFA d_4(r_4);
	BitSet a_4 = alphabetof(d_4);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar temp_1(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_0),opt.block());
	_x << temp_1;
	StringVar temp_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_0;
	StringVar var_4(*this,opt.minlength(),opt.length(),opt.width(),interof(interof(Full,a_1),a_2),opt.block());
	_x << var_4;
	StringVar var_3(*this,opt.minlength(),opt.length(),opt.width(),interof(interof(Full,a_3),a_4),opt.block());
	_x << var_3;
	IntVar len_var_4(*this,opt.minlength(),opt.length());
	_l << len_var_4;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	concat(*this,var_3,str2IntArgs("z"),temp_0);
	concat(*this,temp_0,var_4,temp_1);
	extensional(*this,temp_1,d_0);
	extensional(*this,var_4,d_1);
	extensional(*this,var_4,d_2);
	extensional(*this,var_3,d_3);
	extensional(*this,var_3,d_4);
	length(*this,var_4,len_var_4);
	rel(*this, 0 <= len_var_4);

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/norn/HammingDistance/norn-benchmark-189", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

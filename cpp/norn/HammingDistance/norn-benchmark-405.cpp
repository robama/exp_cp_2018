/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), 'b', 'b'], [('re.*',), [('str.to.re',), 'b']]]]
[('assert',), [('str.in.re',), [('str.++',), 'b', 'a'], [('re.*',), [('str.to.re',), 'a']]]]
[('assert',), [('and',), [('and',), [('and',), [('and',), [('<=',), [('+',), [('str.len',), ('var_0',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_0',)], [('-',), 1]]]], [('and',), [('<=',), [('str.len',), ('var_6',)], 0], [('<=',), 0, [('str.len',), ('var_6',)]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_2',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_2',)], [('-',), 1]]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_3',)], [('-',), 2]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_3',)], [('-',), 2]]]]]]
[('check-sat',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = *(REG(char2val('b')));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *(REG(char2val('a')));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar var_6(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_6;
	StringVar var_3(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_3;
	StringVar var_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_2;
	StringVar var_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_0;
	IntVar len_var_3(*this,opt.minlength(),opt.length());
	_l << len_var_3;
	IntVar len_var_2(*this,opt.minlength(),opt.length());
	_l << len_var_2;
	IntVar len_var_0(*this,opt.minlength(),opt.length());
	_l << len_var_0;
	IntVar len_var_6(*this,opt.minlength(),opt.length());
	_l << len_var_6;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	extensional(*this,str2IntArgs("bb"),d_0);
	extensional(*this,str2IntArgs("ba"),d_1);
	length(*this,var_0,len_var_0);
	rel(*this, (len_var_0) + (-1) <= 0);
	length(*this,var_0,len_var_0);
	rel(*this, 0 <= (len_var_0) + (-1));
	length(*this,var_6,len_var_6);
	rel(*this, len_var_6 <= 0);
	length(*this,var_6,len_var_6);
	rel(*this, 0 <= len_var_6);
	length(*this,var_2,len_var_2);
	rel(*this, (len_var_2) + (-1) <= 0);
	length(*this,var_2,len_var_2);
	rel(*this, 0 <= (len_var_2) + (-1));
	length(*this,var_3,len_var_3);
	rel(*this, (len_var_3) + (-2) <= 0);
	length(*this,var_3,len_var_3);
	rel(*this, 0 <= (len_var_3) + (-2));

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/norn/HammingDistance/norn-benchmark-405", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

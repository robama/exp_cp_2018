/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), 'z', ('var_6',)], [('re.++',), [('re.*',), [('re.union',), [('re.union',), [('str.to.re',), 'z'], [('str.to.re',), 'b']], [('re.++',), [('str.to.re',), 'a'], [('re.union',), [('str.to.re',), 'z'], [('str.to.re',), 'a']]]]], [('str.to.re',), 'a']]]]
[('assert',), [('str.in.re',), [('str.++',), ('var_5',), 'z', ('var_6',)], [('re.*',), [('re.++',), [('re.union',), [('str.to.re',), 'z'], [('str.to.re',), 'a']], [('re.++',), [('re.*',), [('str.to.re',), 'z']], [('str.to.re',), 'a']]]]]]
[('assert',), [('str.in.re',), ('var_6',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), ('var_5',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('and',), [('and',), [('and',), [('<=',), [('+',), [('str.len',), ('var_1',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_1',)], [('-',), 1]]]], [('and',), [('<=',), [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_6',)]]], [('-',), 1]], 0], [('<=',), 0, [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_6',)]]], [('-',), 1]]]]], [('<=',), 0, [('str.len',), ('var_6',)]]]]
[('check-sat',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = (*(((REG(char2val('z'))) | (REG(char2val('b')))) | ((REG(char2val('a'))) + ((REG(char2val('z'))) | (REG(char2val('a'))))))) + (REG(char2val('a')));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *(((REG(char2val('z'))) | (REG(char2val('a')))) + ((*(REG(char2val('z')))) + (REG(char2val('a')))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	REG r_2 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_2(r_2);
	BitSet a_2 = alphabetof(d_2);
	REG r_3 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_3(r_3);
	BitSet a_3 = alphabetof(d_3);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar temp_1(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_0),opt.block());
	_x << temp_1;
	StringVar temp_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_0;
	StringVar temp_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << temp_2;
	StringVar temp_3(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_1),opt.block());
	_x << temp_3;
	StringVar var_6(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_2),opt.block());
	_x << var_6;
	StringVar var_5(*this,opt.minlength(),opt.length(),opt.width(),interof(Full,a_3),opt.block());
	_x << var_5;
	StringVar var_3(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_3;
	StringVar var_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_1;
	IntVar len_var_3(*this,opt.minlength(),opt.length());
	_l << len_var_3;
	IntVar len_var_1(*this,opt.minlength(),opt.length());
	_l << len_var_1;
	IntVar len_var_6(*this,opt.minlength(),opt.length());
	_l << len_var_6;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	concat(*this,var_5,str2IntArgs("z"),temp_0);
	concat(*this,temp_0,var_6,temp_1);
	extensional(*this,temp_1,d_0);
	concat(*this,var_5,str2IntArgs("z"),temp_2);
	concat(*this,temp_2,var_6,temp_3);
	extensional(*this,temp_3,d_1);
	extensional(*this,var_6,d_2);
	extensional(*this,var_5,d_3);
	length(*this,var_1,len_var_1);
	rel(*this, (len_var_1) + (-1) <= 0);
	length(*this,var_1,len_var_1);
	rel(*this, 0 <= (len_var_1) + (-1));
	length(*this,var_3,len_var_3);
	length(*this,var_6,len_var_6);
	rel(*this, ((len_var_3) + ((-1) * (len_var_6))) + (-1) <= 0);
	length(*this,var_3,len_var_3);
	length(*this,var_6,len_var_6);
	rel(*this, 0 <= ((len_var_3) + ((-1) * (len_var_6))) + (-1));
	length(*this,var_6,len_var_6);
	rel(*this, 0 <= len_var_6);

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/norn/HammingDistance/norn-benchmark-87", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

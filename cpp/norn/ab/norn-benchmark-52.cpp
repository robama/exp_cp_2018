/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), ('var_4',), [('re.*',), [('str.to.re',), 'b']]]]
[('assert',), [('str.in.re',), ('var_4',), [('str.to.re',), '']]]
[('assert',), [('str.in.re',), ('var_4',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('and',), [('and',), [('and',), [('and',), [('and',), [('and',), [('<=',), [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_4',)]]], [('-',), 1]], 0], [('<=',), 0, [('+',), [('+',), [('str.len',), ('var_3',)], [('*',), [('-',), 1], [('str.len',), ('var_4',)]]], [('-',), 1]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_0',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_0',)], [('-',), 1]]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_1',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_1',)], [('-',), 1]]]]], [('and',), [('<=',), [('+',), [('+',), [('str.len',), ('var_2',)], [('*',), [('-',), 1], [('str.len',), ('var_4',)]]], [('-',), 2]], 0], [('<=',), 0, [('+',), [('+',), [('str.len',), ('var_2',)], [('*',), [('-',), 1], [('str.len',), ('var_4',)]]], [('-',), 2]]]]], [('<=',), 0, [('+',), [('str.len',), ('var_3',)], [('-',), 2]]]], [('not',), [('not',), [('exists',), [[('v',), ('Int',)]], [('=',), [('*',), ('v',), 2], [('+',), [('str.len',), ('var_3',)], 2]]]]]]]
[('check-sat',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {
	//DFAs:
	REG r_0 = *(REG(char2val('b')));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar var_4(*this,opt.minlength(),opt.length(),opt.width(),interof(interof(Full,a_0),a_1),opt.block());
	_x << var_4;
	StringVar var_3(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_3;
	StringVar var_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_2;
	StringVar var_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_1;
	StringVar var_0(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << var_0;
	IntVar v(*this,opt.minlength(),opt.length());
	_l << v;
	IntVar len_var_3(*this,opt.minlength(),opt.length());
	_l << len_var_3;
	IntVar len_var_2(*this,opt.minlength(),opt.length());
	_l << len_var_2;
	IntVar len_var_1(*this,opt.minlength(),opt.length());
	_l << len_var_1;
	IntVar len_var_0(*this,opt.minlength(),opt.length());
	_l << len_var_0;
	IntVar len_var_4(*this,opt.minlength(),opt.length());
	_l << len_var_4;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	extensional(*this,var_4,d_0);
	length(*this,var_4,0);
	extensional(*this,var_4,d_1);
	length(*this,var_3,len_var_3);
	length(*this,var_4,len_var_4);
	rel(*this, ((len_var_3) + ((-1) * (len_var_4))) + (-1) <= 0);
	length(*this,var_3,len_var_3);
	length(*this,var_4,len_var_4);
	rel(*this, 0 <= ((len_var_3) + ((-1) * (len_var_4))) + (-1));
	length(*this,var_0,len_var_0);
	rel(*this, (len_var_0) + (-1) <= 0);
	length(*this,var_0,len_var_0);
	rel(*this, 0 <= (len_var_0) + (-1));
	length(*this,var_1,len_var_1);
	rel(*this, (len_var_1) + (-1) <= 0);
	length(*this,var_1,len_var_1);
	rel(*this, 0 <= (len_var_1) + (-1));
	length(*this,var_2,len_var_2);
	length(*this,var_4,len_var_4);
	rel(*this, ((len_var_2) + ((-1) * (len_var_4))) + (-2) <= 0);
	length(*this,var_2,len_var_2);
	length(*this,var_4,len_var_4);
	rel(*this, 0 <= ((len_var_2) + ((-1) * (len_var_4))) + (-2));
	length(*this,var_3,len_var_3);
	rel(*this, 0 <= (len_var_3) + (-2));
	length(*this,var_3,len_var_3);
	rel(*this,(v) * (2) == (len_var_3) + (2));

        
        post_brancher(x,opt);
      }
    	
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}
    	
    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
      
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };
    
    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<String::StringVarImp> disposer;
	StringOptions opt("Benchmark::/home/roberto/exp_cp_2018/smt/norn/ab/norn-benchmark-52", atoi(argv[1]));

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);
      
      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    

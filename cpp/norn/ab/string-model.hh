#ifndef __STRING_MODEL_HH__
#define __STRING_MODEL_HH__

#include <iostream>

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/string.hh>

using namespace Gecode;

using Gecode::String::Full;
using Gecode::String::Empty;
using Gecode::String::BitSet;
using Gecode::String::interof;
using Gecode::String::unionof;
using Gecode::String::singletonfor;

/// \brief %Options for %Golf example
class StringOptions : public Options {
protected:
  Driver::IntOption _length; //< maximum string length
  Driver::IntOption _block; //< blocksize for list representations
  Driver::IntOption _width; //< width of the alphabet
  Driver::IntOption _min; //< width of the alphabet
  Driver::BoolOption _zero; // allow zero length strings
public:
  /// Constructor
  StringOptions(const char* name, int length = 500)
    : Options(name),
      _length("-length","maximum string length", length),
      _block("-block","blocksize for lists",16),
      _width("-width","width of the alphabet",63),
      _min("-min","smallest ascii value of any symbol", 32),
      _zero("-zero","allow zero length strings",true) {
    add(_length);
    add(_block);
    add(_width);
    add(_min);
    add(_zero);
  }
  /// Return max string length
  int length(void) const { return _length.value(); }
  /// Return blocksize
  int block(void) const { return _block.value(); }
  /// Return alphabet width
  int width(void) const { return _width.value(); }
  /// Return min symbol ascii code
  int min(void) const { return _min.value(); }
  /// Return zero length flag
  bool zero(void) const { return _zero.value(); }
  /// Return least allowable length
  int minlength(void) const { return _zero.value()?0:1; }
  
  /// set default min symbol ascii code
  void min(int m) { _min.value(m); }
};

class StringModel : public Script {
protected:
  IntVarArray l;
  StringVarArray x;
  int offset;
  
  void post_brancher(StringVarArgs y, const StringOptions& opt) {
    switch(opt.branching()) {
    case BR_WMIN_SPLIT_MIN:
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMAX_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_MIN_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMAX_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_MIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMAX_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_MIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_WMAX_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_MIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MED); break;
		}
  }
  
public:
  enum {BR_WMIN_SPLIT_MIN, BR_WMAX_SPLIT_MIN, BR_MIN_SPLIT_MIN, 
        BR_WMIN_SPLIT_MED, BR_WMAX_SPLIT_MED, BR_MIN_SPLIT_MED, 
        BR_WMIN_MIN_MIN,   BR_WMAX_MIN_MIN,   BR_MIN_MIN_MIN, 
        BR_WMIN_MIN_MED,   BR_WMAX_MIN_MED,   BR_MIN_MIN_MED};
        
  
	StringModel(const StringOptions& opt)
    : Script(opt), offset(opt.min() - 1) {}
	StringModel(bool share, StringModel& s)
	: Script(share,s), offset(s.offset) {
    x.update(*this, share, s.x);
    l.update(*this, share, s.l);
	}

  static void standardOptions(StringOptions& opt) {
    opt.branching(StringModel::BR_WMIN_SPLIT_MIN, "widthmin-split-min", "branching: index - width min; val - split; length - min");
    opt.branching(StringModel::BR_WMAX_SPLIT_MIN, "widthmax-split-min", "branching: index - width max; val - split; length - min");
    opt.branching(StringModel::BR_MIN_SPLIT_MIN, "min-split-min", "branching: index - min; val - split; length - min");
    opt.branching(StringModel::BR_WMIN_SPLIT_MED, "widthmin-split-med", "branching: index - width min; val - split; length - med");
    opt.branching(StringModel::BR_WMAX_SPLIT_MED, "widthmax-split-med", "branching: index - width max; val - split; length - med");
    opt.branching(StringModel::BR_MIN_SPLIT_MED, "min-split-med", "branching: index - min; val - split; length - med");
    opt.branching(StringModel::BR_WMIN_MIN_MIN, "widthmin-min-min", "branching: index - width min; val - min; length - min");
    opt.branching(StringModel::BR_WMAX_MIN_MIN, "widthmax-min-min", "branching: index - width max; val - min; length - min");
    opt.branching(StringModel::BR_MIN_MIN_MIN, "min-min-min", "branching: index - min; val - min; length - min");
    opt.branching(StringModel::BR_WMIN_MIN_MED, "widthmin-min-med", "branching: index - width min; val - min; length - med");
    opt.branching(StringModel::BR_WMAX_MIN_MED, "widthmax-min-med", "branching: index - width max; val - min; length - med");
    opt.branching(StringModel::BR_MIN_MIN_MED, "min-min-med", "branching: index - min; val - min; length - med");
    opt.branching(StringModel::BR_WMIN_SPLIT_MIN);
  }
  
  int char2val(char a) const {
    return ((int)a) - offset;
  }
  
  char val2char(int n) const {
    return n + offset;
  }

  IntArgs str2IntArgs(std::string s) const {
    IntArgs arg;
    for (size_t i = 0; i < s.length(); i++)
      arg << char2val(s[i]);
    return arg;
  }
  
  std::string IntArg2str(IntArgs arg) const {
    std::string s;
    for (int i = 0; i < arg.size(); i++)
      s[i] = val2char(arg[i]);
    return s;
  }
  
  BitSet alphabetof(const DFA& dfa) const {
    BitSet alpha = Empty;
    for (DFA::Symbols sym(dfa); sym(); ++sym) {
      alpha = unionof(alpha,singletonfor(sym.val()));
    }
    return alpha;
  }
  
	virtual Space* copy(bool share) {
		return new StringModel(share,*this);
	}

	virtual void print(std::ostream& os) const {
    os << x << ", " << l << std::endl;
	}
};
#endif